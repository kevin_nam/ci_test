package com.kevintest.optus.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Adapter for the ViewPager in Point 2
 */
public class PointTwoPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public PointTwoPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return fragments.indexOf(object);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}