# Mock App
This app was made for technical evaluation only. 

## Introduction
This app has two main fragments with the following features:
  - RecyclerView, ViewPager, Spinner with Adapter
  - LinearLayout, FrameLayout, RelativeLayout
  - TextView, Button, RadioGroup/RadioButton
  - Activity, Fragment, GoogleMap Fragment, Sliding Drawer
  - Portrait/Landscape mode
  - Multiple flavors depending on screen densities
 

## Screeenshots
<img src="screenshots/screenshot-1.png" height="400" alt="Screenshot"/> <img src="screenshots/screenshot-2.png" height="200" alt="Screenshot"/>
<img src="screenshots/screenshot-3.png" height="400" alt="Screenshot"/> <img src="screenshots/screenshot-4.png" height="200" alt="Screenshot"/>

## Get started
To build this project, use the "gradlew build" command or use "Import Project" in Android Studio.
```sh
./gradlew build
```

## License
MIT
**Free Software!**